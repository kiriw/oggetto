const path = require('path');

module.exports = {
    devtool: 'source-map',
    entry: [
    './src/app.js'
    ],
    output: {
        path: path.join(__dirname, 'build'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [
        {
            test:    /\.js$/,
            loader:  'babel-loader?presets[]=env'
        },
        {
            test: /\.css$/,
            loaders: ['style-loader', 'css-loader']
        },
        {
            test: /\.scss$/,
            loaders: ['style-loader', 'css-loader', 'sass-loader']
        }
        ]
    }
};
