import $ from 'jquery';
import CitySelector from './CitySelector';

const createButton = document.getElementById('createCitySelector');
const destroyButton = document.getElementById('destroyCitySelector');


destroyButton.setAttribute('disabled',true);
let citySelector;

createButton.onclick = function() {
	citySelector = new CitySelector({
		elementId: 'citySelector',
		regionsUrl: 'http://localhost:3000/regions',
		localitiesUrl: 'http://localhost:3000/localities',
		saveUrl: 'http://localhost:3000/selectedRegions'
	});
	
	$('#info').show();
	
	citySelector.on('change',function(data){
		$('#regionText').text(data.id);
		$('#localityText').text(data.locality);
	})
	this.setAttribute('disabled',true);
	destroyButton.removeAttribute('disabled');
}

destroyButton.onclick = function() {
	citySelector.destroy();
	createButton.removeAttribute('disabled');
	this.setAttribute('disabled',true);
	$('#regionText').text('');
	$('#localityText').text('');
	$('#info').hide();
}


