'use strict'

import './style.scss';

import $ from           'jquery';
import eventMixin from  '../EventMixin';

const listClassName   = 'selector__list';
const itemClassName   = 'selector__item';
const buttonClassName = 'selector__button';

let CitySelector = function({elementId, regionsUrl, localitiesUrl, saveUrl}) {

	this.selected = {};
	this.$node = $(`#${elementId}`);
	const _ = this;

	let $regionsList = $('<ul>', {class: listClassName});
	let $localitiesList = $('<ul>', {class: listClassName});
	let $saveButton = $('<button>', {
		class: buttonClassName,
		text: 'Сохранить',
		disabled: true
	});
	let $startButton = $('<button>', {
		class: buttonClassName,
		text: 'Выбрать регион',
	});

	let getData = function(url,id) {
		let _url = url + ((id) ? `/${id}` : '');
		return new Promise(response => {
			fetch(_url)
			.then(data => data.json())
			.then(data => response(data))
		})
	}

	let saveData = function() {
		_.trigger('selectLocality', _.selected);
		$.ajax({
			url: saveUrl,
			method: 'post',
			async: false,
			data: JSON.stringify(_.selected)
		}).then(() => window.location = saveUrl)
	}

	let init = function() {
		getData(regionsUrl).then(data => {
			$startButton.hide();
			_.$node.append($regionsList)
			.append($localitiesList)
			.append($saveButton);
			$saveButton.hide(); 
			createRegionsList(data)
		})
	}

	let createRegionsList = function(data) {
		let list = data.map(item => {
			return $('<li>', {
				class: itemClassName,
				value: item.id,
				text: item.title
			});
		});
		$regionsList.append(list);
	}

	let createLocalitiesList = function(data) {
		$localitiesList.empty();
		let list = data.map(item => $('<li>', {class: itemClassName, text: item}));
		$localitiesList.append(list);
	}

	let getLocalities = function(id) {
		return new Promise(response => {
			getData(localitiesUrl, id)
			.then(data => response(data.list))
		})
	}

	$startButton.on('click', init)
	$saveButton.on('click', saveData)
	$localitiesList.on('click', `.${itemClassName}`, function() {
		let locality = $(this).text();
		_.selected.locality = locality;
		$saveButton.attr('disabled',false);
	});
	$regionsList.on('click', `.${itemClassName}`, function() {
		let id = $(this).val();
		let region = $(this).text();
		$saveButton.show();
		getLocalities(id).then(createLocalitiesList);
		_.selected = {
			region: region,
			id: id,
			locality: ''
		};
		$saveButton.attr('disabled',true);
	});
	_.$node.on('click', `.${itemClassName}`, function() {
		let $item = $(this);
		$item.siblings().removeClass('active');
		$item.addClass('active');
		_.trigger('change', _.selected);
	})
	
	_.$node
	.append($startButton); 
}

for(let key in eventMixin) {
	CitySelector.prototype[key] = eventMixin[key];
}

CitySelector.prototype.getSelected = function() {
	return this.selected
}

CitySelector.prototype.destroy = function() {
	this.$node.empty();
}

export default CitySelector
